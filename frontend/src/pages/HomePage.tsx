import React, { useState, useCallback } from 'react';
import { Link, useLocation } from 'react-router-dom';
import Header from '../components/Header';
import Page1 from './Page1';
import Page2 from './Page2';
import Page3 from './Page3';
import Page4 from './Page4';
import Page5 from './Page5';

const pages = ['/page1', '/page2', '/page3', '/page4', '/page5'];

const HomePage: React.FC = () => {
  const location = useLocation();
  const pathname = location.pathname;

  const [currentPage, setCurrentPage] = useState(() => {
    let page = '/' + pathname.split('/').pop();
    if (page === '/home' || page === '/') {
      page = '/page1';
    }
    return page;
  });

  const handlePageChange = useCallback((page: string) => {
    window.history.pushState(null, '', page);
    setCurrentPage(page);
  }, []);

  return (
    <div className="container">
      <Header />

      <div className="jumbotron mt-4">
        <h3 className="display-4">Добро пожаловать на главную страницу!</h3>
        <p className="lead">Здесь вы можете насладиться прекрасным контентом и удобным интерфейсом.</p>
        <hr className="my-4" />
        <p>Демонстрация react-router, покликайте по ссылкам!</p>
        <div className="route-container">
          <div className="buttons-container">
            {pages.map((pageLink) => (
              <Link
                key={pageLink}
                to={pageLink}
                className={`btn btn-primary btn-lg ${currentPage === pageLink ? 'active' : ''}`}
                onClick={() => handlePageChange(pageLink)}
              >
                {`Страница ${pages.indexOf(pageLink) + 1}`}
              </Link>
            ))}
          </div>
            <div className="page-frame">
              {currentPage === '/page1' && <Page1 />}
              {currentPage === '/page2' && <Page2 />}
              {currentPage === '/page3' && <Page3 />}
              {currentPage === '/page4' && <Page4 />}
              {currentPage === '/page5' && <Page5 />}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
