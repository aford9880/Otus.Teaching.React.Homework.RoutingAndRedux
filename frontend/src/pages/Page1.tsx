import React from 'react';

const Page1: React.FC = () => {
  return (
    <div>
      <h3 className="page-number">Страница 1</h3>
      <div className="page-content">
        <p>Добро пожаловать на страницу 1!</p>
        <p>Это пример контента для первой страницы.</p>
      </div>
    </div>
  );
};

export default Page1;