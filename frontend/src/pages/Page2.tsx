import React from 'react';

const Page2: React.FC = () => {
  return (
    <div>
      <h3 className="page-number">Страница 2</h3>
      <div className="page-content">
        <p>Добро пожаловать на страницу 2!</p>
        <p>Это пример контента для второй страницы.</p>
      </div>
    </div>
  );
};

export default Page2;