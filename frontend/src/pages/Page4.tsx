import React from 'react';

const Page4: React.FC = () => {
  return (
    <div>
      <h3 className="page-number">Страница 4</h3>
      <div className="page-content">
        <p>Добро пожаловать на страницу 4!</p>
        <p>Это пример контента для четвертой страницы.</p>
      </div>
    </div>
  );
};

export default Page4;