import React from 'react';

const Page5: React.FC = () => {
  return (
    <div>
      <h3 className="page-number">Страница 5</h3>
      <div className="page-content">
        <p>Добро пожаловать на страницу 5!</p>
        <p>Это пример контента для пятой страницы.</p>
      </div>
    </div>
  );
};

export default Page5;