import React from 'react';

const Page3: React.FC = () => {
  return (
    <div>
      <h3 className="page-number">Страница 3</h3>
      <div className="page-content">
        <p>Добро пожаловать на страницу 3!</p>
        <p>Это пример контента для третьей страницы.</p>
      </div>
    </div>
  );
};

export default Page3;