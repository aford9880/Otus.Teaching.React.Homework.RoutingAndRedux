import Cookies from 'js-cookie';

interface AuthState {
  isLoggedIn: boolean;
  isRegistered: boolean;
}

const initialState: AuthState = {
  isLoggedIn: !!Cookies.get('token'),
  isRegistered: false,
};

const authReducer = (state = initialState, action: any): AuthState => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
        return {
        ...state,
        isLoggedIn: true,
      };
    case 'LOGOUT':
      Cookies.remove('token');
      return {
        ...state,
        isLoggedIn: false,
      };
    case 'REGISTER_SUCCESS':
        return {
        ...state,
        isLoggedIn: true,
        isRegistered: true,
      };
    default:
      return state;
  }
};

export default authReducer;