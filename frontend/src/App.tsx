import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './css/index.css';
import LoginForm from './components/LoginForm';
import RegisterForm from './components/RegisterForm';
import HomePage from './pages/HomePage';
import Page404 from './pages/Page404';
import { useSelector } from 'react-redux';
import { RootState } from './store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const App: React.FC = () => {

  const isLoggedIn = useSelector((state: RootState) => state.auth.isLoggedIn);
  
  return (
    <React.StrictMode>
      <Router>
        <ToastContainer />
        <Routes>
          {isLoggedIn ? (
            <>
              <Route path="/page1" element={<HomePage />} />
              <Route path="/page2" element={<HomePage />} />
              <Route path="/page3" element={<HomePage />} />
              <Route path="/page4" element={<HomePage />} />
              <Route path="/page5" element={<HomePage />} />
              <Route path="/home" element={<HomePage />} />
              <Route path="/" element={<HomePage />} />
            </>
          ) : (
            <>
              <Route
                path="/"
                element={<LoginForm />}
              />
              <Route
                path="/login"
                element={<LoginForm />}
              />
              <Route path="/register" element={<RegisterForm />} />
            </>
          )}
          <Route path="*" element={<Page404 />} />
        </Routes>
      </Router>
    </React.StrictMode>
  );
};

export default App;