import React from 'react';
import Cookies from 'js-cookie';
import '../css/header.css';
import { useDispatch } from 'react-redux';
import { logout } from '../actions/authActions';
import { useNavigate } from 'react-router-dom';

const Header: React.FC = () => {
  
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleLogout = () => {
    Cookies.remove('token');
    dispatch(logout());
    navigate('/login');
    console.log('Пользователь разлогинился');
  };

  const token = Cookies.get('token');
  const truncatedToken = token ? token.substring(0, 10) + '...' : '';

  return (
    <div className="header-container">
      {token ? (
        <>
          <div className="token-info">
            <h3>Пользователь авторизован, токен: {truncatedToken}</h3>
          </div>
          <button className="logout-button" onClick={handleLogout}>
            Выйти
          </button>
        </>
      ) : (
        <h3>Пользователь не авторизован</h3>
      )}
    </div>
  );
};

export default Header;