import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios, { AxiosError } from 'axios';
import Cookies from 'js-cookie';
import '../css/forms.css';
import { useDispatch } from 'react-redux';
import { registerSuccess } from '../actions/authActions';

interface RegisterResponse {
  message: string;
  token: string;
}

const RegisterForm: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState('');
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setErrorMessage('');
    setMessage('');
    if (name === 'username') {
      setUsername(value);
    } else if (name === 'password') {
      setPassword(value);
    } else if (name === 'confirmPassword') {
      setConfirmPassword(value);
    } else if (name === 'dateOfBirth') {
      setDateOfBirth(value);
    }
  };

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();

    if (password !== confirmPassword) {
      setErrorMessage('Пароли не совпадают.');
      return;
    }

    try {
      const response = await axios.post<RegisterResponse>('/api/register', {
        username,
        password,
        dateOfBirth,
      });

      setMessage(response.data.message);
      setErrorMessage('');
      const token = response.data.token;
      Cookies.set('token', token, { expires: 7 });
      dispatch(registerSuccess());
      navigate('/home');
    } catch (error: unknown) {
      console.error(error);

      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response && axiosError.response.status === 400) {
          setErrorMessage('Необходимо заполнить поля Логин, Пароль и Дата рождения.');
        } else {
          setErrorMessage('Произошла ошибка при запросе к серверу!');
        }
      } else {
        setErrorMessage('Произошла неизвестная ошибка.');
      }
    }
  };

  return (
    <div className="o-form-container">
      <form onSubmit={handleSubmit} className="o-form">
        <div className="o-form-group">
          <h6>Введите учетные данные для регистрации</h6>
          <div>Уже зарегистрированы? <Link to="/login">Авторизация</Link></div>
          <label>
            Логин:
            <input
              type="text"
              name="username"
              value={username}
              onChange={handleInputChange}
            />
          </label>
        </div>
        <div className="o-form-group">
          <label>
            Пароль:
            <input
              type="password"
              name="password"
              value={password}
              onChange={handleInputChange}
            />
          </label>
        </div>
        <div className="o-form-group">
          <label>
            Подтвердите пароль:
            <input
              type="password"
              name="confirmPassword"
              value={confirmPassword}
              onChange={handleInputChange}
            />
          </label>
        </div>
        <div className="o-form-group">
          <label>
            Дата рождения:
            <input
              type="date"
              name="dateOfBirth"
              value={dateOfBirth}
              onChange={handleInputChange}
            />
          </label>
        </div>
        {message && <p>{message}</p>}
        {errorMessage && <p className="o-error-message">{errorMessage}</p>}
        <button type="submit">Зарегистрироваться</button>
      </form>
    </div>
  );
};

export default RegisterForm;
