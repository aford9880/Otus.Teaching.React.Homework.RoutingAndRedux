import React, { useState } from 'react';
import axios, { AxiosError } from 'axios';
import Cookies from 'js-cookie';
import '../css/forms.css';
import { useNavigate, Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { loginSuccess } from '../actions/authActions';

interface LoginResponse {
  token: string;
}

const LoginForm: React.FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const dispatch = useDispatch();
  const navigate = useNavigate();
  
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setErrorMessage('');
    setMessage('');
    if (name === 'username') {
      setUsername(value);
    } else if (name === 'password') {
      setPassword(value);
    }
  };

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
  
    try {
      const response = await axios.post<LoginResponse>('/api/auth', {
        username,
        password,
      });
  
      const token = response.data.token;
      setMessage('Данные успешно отправлены на сервер!');
      setErrorMessage('');
      Cookies.set('token', token, { expires: 7 });
      dispatch(loginSuccess());
      navigate('/home');
    } catch (error: unknown) {
      console.error(error);
  
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response && axiosError.response.status === 400) {
          setErrorMessage('Необходимо заполнить поля UserName и Password.');
        } else {
          setErrorMessage('Произошла ошибка при запросе к серверу!');
        }
      } else {
        setErrorMessage('Произошла неизвестная ошибка.');
      }
    }
  };

  return (
    <div className="o-form-container">
      <form onSubmit={handleSubmit} className="o-form">
        <div className="o-form-group">
          <h6>Введите учетные данные</h6>
          <div>Не зарегистрированы? <Link to="/register">Регистрация</Link></div>
          <label>
            Username:
            <input
              type="text"
              name="username"
              value={username}
              onChange={handleInputChange}
            />
          </label>
        </div>
        <div className="o-form-group">
          <label>
            Password:
            <input
              type="password"
              name="password"
              value={password}
              onChange={handleInputChange}
            />
          </label>
        </div>
        {message && <p>{message}</p>}
        {errorMessage && <p className="o-error-message">{errorMessage}</p>}
        <button type="submit">Log In</button>
      </form>
    </div>
  );
};

export default LoginForm;
