import { toast } from "react-toastify";

export const loginSuccess = () => {
    toast.success('REDUX ACTIONS: Вы успешно вошли в систему!'); 
    return {
      type: 'LOGIN_SUCCESS',
    };
  };
  
  export const logout = () => {
    toast.info('REDUX ACTIONS: Вы успешно вышли из системы.');
    return {
      type: 'LOGOUT',
    };
  };
  
  export const registerSuccess = () => {
    toast.success('REDUX ACTIONS: Регистрация успешно завершена!');
    return {
      type: 'REGISTER_SUCCESS',
    };
  };