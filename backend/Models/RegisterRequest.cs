﻿namespace Server.Models
{
    public class RegisterRequest
    {
        public string? Username { get; set; }
        public string? Password { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
